from collections import defaultdict
import graph_examples as ge
from dfs import dfs
from bfs import bfs
import unittest


class TestForDfsAndBfs(unittest.TestCase):
    def test_dfs_empty_input(self):
        g = defaultdict(list)
        res = dfs(g)
        assert len(res) == 0

    def test_bfs_empty_input(self):
        g = defaultdict(list)
        res = bfs(g)
        assert len(res) == 0

    def test_bfs_example_graph(self):
        g = ge.return_example_graph()
        res = bfs(g)
        assert len(res) == 4
        rightans = ["A", "B", "C", "D"]
        for index in range(len(rightans)):
            assert res[index] == rightans[index]

    def test_dfs_example_graph(self):
        g = ge.return_example_graph()
        res = dfs(g)
        assert len(res) == 4
        rightans = ["A", "C", "D", "B"]
        for index in range(len(rightans)):
            assert res[index] == rightans[index]

    def test_bfs_example_matrix(self):
        matr = ge.return_example_matrix_graph()
        names = ge.return_example_names_graph()
        g = ge.get_graph_from_matrix(matr, names)
        res = bfs(g)
        assert len(res) == 4
        rightans = ["A", "B", "C", "D"]
        for index in range(len(rightans)):
            assert res[index] == rightans[index]

    def test_dfs_example_matrix(self):
        matr = ge.return_example_matrix_graph()
        names = ge.return_example_names_graph()
        g = ge.get_graph_from_matrix(matr, names)
        res = dfs(g)
        assert len(res) == 4
        rightans = ["A", "C", "D", "B"]
        for index in range(len(rightans)):
            assert res[index] == rightans[index]

    def test_bfs_example_dict(self):
        g = ge.get_graph_from_dict()
        res = bfs(g)
        assert len(res) == 8
        rightans = ["1", "2", "3", "4", "5", "6", "7", "8"]
        for index in range(len(rightans)):
            assert res[index] == rightans[index]

    def test_dfs_example_dict(self):
        g = ge.get_graph_from_dict()
        res = dfs(g)
        assert len(res) == 8
        rightans = ["1", "4", "3", "7", "8", "2", "6", "5"]
        for index in range(len(rightans)):
            assert res[index] == rightans[index]

    def test_from_matrix_conversion(self):
        right_g = ge.return_example_graph()
        right_g["D"].extend([])
        ex_names = ge.return_example_names_graph()
        ex_matrix = ge.return_example_matrix_graph()
        g = ge.get_graph_from_matrix(ex_matrix, ex_names)
        assert g == right_g

    def test_from_dict_conversion(self):
        right_g = defaultdict(list)
        right_g["1"].extend(["2", "3", "4"])
        right_g["2"].extend(["5", "6"])
        right_g["3"].extend(["7"])
        right_g["7"].extend(["8"])
        g = ge.get_graph_from_dict()
        assert g == right_g

    def test_from_empty_dict(self):
        right_g = defaultdict(list)
        mydict = {}
        g = ge.get_graph_from_dict(mydict)
        assert g == right_g

    def test_from_empty_matrix(self):
        right_g = defaultdict(list)
        mymatr = []
        mynames = []
        g = ge.get_graph_from_matrix(mymatr, mynames)
        assert g == right_g
