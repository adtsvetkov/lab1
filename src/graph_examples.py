from collections import defaultdict
from typing import DefaultDict, List, Dict


def return_example_names_graph() -> List[str]:
    names = ["A", "B", "C", "D"]
    return names


def return_example_matrix_graph() -> List[List[int]]:
    matrix = [[0, 1, 1, 0],
              [0, 0, 1, 0],
              [0, 0, 0, 1],
              [0, 0, 0, 0]]
    return matrix


def get_graph_from_matrix(matrix: List[List[int]],
                          names: List[str]) \
        -> DefaultDict[str, List[str]]:
    g = defaultdict(list)
    for i in range(len(names)):
        connected_vertex = []
        for j in range(len(matrix[i])):
            if matrix[i][j] == 1:
                connected_vertex.append(names[j])
        g[names[i]].extend(connected_vertex)
    return g


def return_example_graph() -> DefaultDict[str, List[str]]:
    g = defaultdict(list)
    g["A"].extend(["B", "C"])
    g["B"].extend(["C"])
    g["C"].extend(["D"])
    return g


def _return_dict_example_graph() -> Dict[str, List[str]]:
    g = {"1": ["2", "3", "4"],
         "2": ["5", "6"],
         "3": ["7"],
         "7": ["8"]}
    return g


def get_graph_from_dict(d: Dict[str, List[str]] = None) \
        -> DefaultDict[str, List[str]]:
    if d is None:
        d = _return_dict_example_graph()
    g: DefaultDict[str, List[str]] = defaultdict(list)
    for letter in d:
        g[letter].extend(d[letter])
    return g
