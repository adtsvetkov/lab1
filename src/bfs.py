from typing import DefaultDict, List
from collections import deque


# works only with G as adjacency list
# we assume that first element of 'defaultdict' is root
def bfs(g: DefaultDict[str, List[str]]) -> List[str]:
    vertexes = []
    if len(g.keys()) != 0:
        root = list(g.keys())[0]
        to_visit = deque([root])
        vertexes.append(root)
        while to_visit:
            vertex = to_visit.popleft()
            for connected in g[vertex]:
                if connected not in to_visit:
                    vertexes.append(connected)
                    to_visit.append(connected)
    # return unique vertexes
    return vertexes
