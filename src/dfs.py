from typing import DefaultDict, List


# works only with G as adjacency list
# we assume that first element of 'defaultdict' is root
def dfs(g: DefaultDict[str, List[str]]) -> List[str]:
    visited = []
    if len(g.keys()) != 0:
        # adding root to stack
        to_visit = [list(g.keys())[0]]
        while to_visit:
            vertex = to_visit.pop()
            if vertex not in visited:
                visited.append(vertex)
                to_visit.extend(g[vertex])
        # return unique vertexes
    return visited
