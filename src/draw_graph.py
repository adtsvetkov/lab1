from typing import DefaultDict, List
import networkx as nx  # type: ignore
import matplotlib.pyplot as plt  # type: ignore
import random


def draw_steps(g: DefaultDict[str, List[str]],
               used: List[str], step_by_step=False,
               msg: str = "") -> None:
    graph = nx.DiGraph()
    for step in range(0, len(used)):
        if step_by_step:
            plt.title("Step №" + str(step+1) + ": "
                      + "You have to close this window "
                        "to go to the next step\n"
                      + msg)
        else:
            plt.title("Animated view of graph\n" + msg)
        graph.add_node(used[step],
                       Position=(random.randrange(0, 100),
                                 random.randrange(0, 100)))
        for node in g:
            for connected in g[node]:
                if connected in used[:(step+1)] and node in used[:(step+1)]:
                    graph.add_edge(node, connected)
        nx.draw(graph, nx.get_node_attributes(graph, 'Position'))
        nx.draw_networkx_nodes(graph,
                               nx.get_node_attributes(graph, 'Position'),
                               node_color="white", edgecolors="black")
        nx.draw_networkx_labels(graph,
                                nx.get_node_attributes(graph, 'Position'),
                                font_size=14, font_color="red")
        if step_by_step:
            plt.show()
        else:
            plt.pause(0.5)
    plt.show()
