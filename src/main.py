import graph_examples as ge
from bfs import bfs
from dfs import dfs
from draw_graph import draw_steps


def start() -> None:
    g_1 = ge.get_graph_from_dict()

    example_names = ge.return_example_names_graph()
    example_matrix = ge.return_example_matrix_graph()
    g_2 = ge.get_graph_from_matrix(example_matrix, example_names)

    g_3 = ge.return_example_graph()

    used_1 = dfs(g_1)
    draw_steps(g_1, used_1, msg="DFS")

    used_2 = bfs(g_2)
    draw_steps(g_2, used_2, msg="BFS")

    used_3 = dfs(g_3)
    draw_steps(g_3, used_3, step_by_step=True, msg="DFS")


if __name__ == "__main__":
    start()
