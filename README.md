# Graph visualization
## Purpose
Depth first search & breadth first search visualization
## Requirements
- python 3.8.x ([get at python.org](https://www.python.org/downloads/))
- additional packages (see in `requirements.txt`) 
## Installation
- Download these files from GitLab or clone this repository via https (don't forget to install git):
    ```
    git clone https://gitlab.com/adtsvetkov/lab1.git
    cd lab1
    ```
    If you want to run application from the `develop` branch:
    ```
    git checkout develop
    ```
- Install dependencies from command line (use full path if necessary):
   ```
   pip3 install -r requirements.txt
   ```
   If you use PyCharm, you can run this command from `lab1` project Terminal. Also don't forget to set `python 3.8.x` as project interpreter.
## Usage
- Run the program from the root folder of repository: `python src/main.py` (use full path if necessary)
  ```
  cd src
  python3 main.py
  ```
- You will see examples of using program. You will see algorithms of breadth first search and depth first search animated and step-by-step for `matrix`-graph, `dictionary`-graph and `defaultdict`-graph.
 
## Explanation
Screens and animation visualize bfs and dfs algorithms-work. You can see graph visualization step by step (`draw_graph(..., step_by_step = True)`) or animated. You should close windows to go to the next steps of algorithm while using step by step mode.

Some graph examples you can find in `graph_examples.py`. Algorithms work only with graphs as adjacency list using `defaultdict`. You can convert your matrix or dictionary graphs to defaultdict using functions named `get_graph_from_matrix()` и `get_graph_from_dict()`.

You can run all unittests provided using `unittest.discover` (use full paths if necessary):
```
python3 -m unittest discover
```

## Example
see "Usage".
- Step-by-step dfs visualization looks like this:
<img src = https://sun9-11.userapi.com/V7Sashy2Fs8OrInxVCc-cI4mOdFwwW6HTFevzw/ECh681p41vk.jpg width = "562" height = "483">  
<img src = https://sun9-43.userapi.com/VTfl8vvzrAOsaNqsbafDek2DnfwnxgCWxj9GJw/iWRTl6Y78fQ.jpg width = "562" height = "483">  
<img src = https://sun9-51.userapi.com/_k-B5QcejTJoXNNWBUx6biH5xkysGYneKlOtpQ/CUIUKeerk60.jpg width = "562" height = "483">  
<img src = https://sun9-28.userapi.com/Sft-R0d_Tw2HfVTh-tFtv1vRreWqw_cO7LNy_w/qFhm2BZAaUA.jpg width = "562" height = "483">  

- Animated bfs visualization looks like this:

<img src = https://sun9-63.userapi.com/QokJjSwWIWiU4KOKf3cmKmEFL9cA7TF_oDkLBQ/nfa3uW-fTpg.jpg width = "562" height = "483">  
<img src = https://sun9-43.userapi.com/5V0MAtI899rOXhmYkkq4oWNSaQ73XZqm23c1JQ/pC4P3C74wdk.jpg width = "562" height = "483">  
<img src = https://sun9-27.userapi.com/tmlW5lfl5Yq1mkviix57rWid6x8aon0hiQdrzA/oTm-L4IX3hU.jpg width = "562" height = "483">  
<img src = https://sun9-59.userapi.com/BxnBryIeZt-hHzVj8y9nXt_HSmpiwyI186mwAg/IiTB19sRdHo.jpg width = "562" height = "483">